let allProducts = [];
window.onload = function () { //alert('load');
    if (sessionStorage.getItem('id') !== null) { // ingelogd
        const navLogout = document.getElementById('loginBtn');
        navLogout.addEventListener('click', logOut);
        navLogout.innerText= "Logout";
        navLogout.href = "homeBootstrap.html";
    }

    // producten inladen
    let currentFile =  window.location.href.split('/')[6].split('?')[0];
    let secondFileName = currentFile.split('#')[0];
    if (secondFileName === 'homeBootstrap.html') { //alert('idd');
        getProducts();
    }

    if (currentFile === 'shoppingCart.html') {
   //     alert('shoppingcart');
        loadShoppingCartItems();
    }

}

function logOut() {
    sessionStorage.removeItem('id');
    alert("Uitgelogd");
}

function getProducts() {
    fetch('https://localhost:44337/api/Product/')
        .then(validateResponse)
        .then(logResult)
        .then(logData)
        .catch(logError);
}

function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

function logResult(result) {
    return result.json();
}

function logData(result) {
    allProducts = JSON.parse(result.Data);
    createProductHtmlElements();
    // alert(result.Data);
    //  console.log(result.Data);
}

function logError(error) {
    console.log('Looks like there was a problem:', error);
}

//let currentProductPage = 0;

function createProductHtmlElements() {
    const products = document.getElementById('productsContainer');
    let completeProductPage = "";

    let index = 0;
    allProducts.forEach(product => {
        let productPage =
            '<div class="col-md-4">' +
            '<div class="product-module">' +
            '<img onclick="productDetails(' + index + ')" class="card-img-top " src="../images/products/' + product.image + '.jpg"/>' + // image
            '<div class="product-name">' +
            '<h5><a href="#">' + product.productname + '</a> </h5>' + // naam
            '</div>' +
            '<div class="price"><h5>€ ' + product.price + '</h5></div>' +   // prijs
            '<div class="card-body">' +
            '<button onclick="addToBasket(' + index +')" class="btn btn-secondary">Add to Basket</button>' +
            '</div>' +
            '</div>' +
            '</div>';
        completeProductPage += productPage;
        index++;
    });
    products.innerHTML = completeProductPage;

    /*
'<div class="col-md-4">' +
    '<div class="product-module">'+
        '<img class="card-img-top " src="../images/smartphone.jpg"/>'+
        '<div class="product-name">'+
            '<h5><a href="#">Apple iPhone 11 Dual eSIM 64GB 4GB RAM White</a> </h5>'+
        '</div>'+
        '<div class="price"><h5>815.00&nbsp;</h5></div>'+
        '<div class="card-body">'+
            '<a href="#" class="btn btn-secondary">Add to Basket</a>'+
        '</div>'+
    '</div>'+
'</div>';
*/
}

function productDetails(index) {
    alert(index);
}

function addToBasket(index) {
    sessionStorage.setItem('product' + index, JSON.stringify(allProducts[index])); // item
    alert('Het product werd toegevoegd aan het winkelmandje');
    console.log(JSON.parse(sessionStorage.getItem('product'+index)));

 //   sessionStorage.setItem('product' + index,  allProducts[index].productname);
  //  console.log(sessionStorage.getItem('product2'));
}

function loadShoppingCartItems() {
    /*
    let table = document.getElementById('shoppingCartTableBody');

    //
    let row = table.insertRow(0);
    //

    let completeTable = "";

    let index = 0;
    allProducts.forEach(product => {
        let tableRow =
    '<tr>' +
        '<td scope="row">'+
            '<div class="p-2">'+
                '<img src="../images/' + product.image + '.jpg" width="70" class="img-fluid rounded shadow-sm">'+ // img
                    '<div class="ml-3 d-inline-block align-middle">'+
                        '<h5 class="mb-0">' +
                            '<a href="#" class="text-dark d-inline-block align-middle">' + product.productname + '</a>' + // name
                        '</h5>' +
                        '<span class="text-muted font-weight-normal font-italic d-block">Category: ' + '</span>'+ // extra optie
                    '</div>'+
            '</div>'+
        '</td>'+
        '<td class="align-middle"><strong>' + product.price + '</strong></td>'+ // price
        '<td class="align-middle"><strong>' + '</strong></td>'+ // quantity
        '<td class="align-middle">'+
            '<button type="button" onclick="removeShoppingCartItem(' + index + ')" class="btn btn-outline-danger btn-xs">'+ // delete
                '<i class="fa fa-trash" aria-hidden="true"></i>'+
            '</button>'+
        '</td>'+
    '</tr>';


        completeTable += tableRow;
        index++;
    });
    table.innerText = completeTable;
*/
    /*alert('shop');
    let cartItems = document.getElementById('shoppingCartContainer');
    cartItems.innerHTML = '<h1>hi</h1>';
    alert('yup');
    let completeProductPage = "";

    let index = 0;
    allProducts.forEach(product => {
        let productPage =
            '<div class="col-md-4">' +
            '<div class="product-module">' +
            '<img onclick="productDetails(' + index + ')" class="card-img-top " src="../images/products/' + product.image + '.jpg"/>' + // image
            '<div class="product-name">' +
            '<h5><a href="#">' + product.productname + '</a> </h5>' + // naam
            '</div>' +
            '<div class="price"><h5>€ ' + product.price + '</h5></div>' +   // prijs
            '<div class="card-body">'
            +'<button type="button" onclick="removeShoppingCartItem(' + index + ')" class="btn btn-outline-danger btn-xs">'+ // delete
        '<i class="fa fa-trash" aria-hidden="true"></i>'+
        '</button>'
            +
            '</div>' +
            '</div>' +
            '</div>';
        completeProductPage += productPage;
        index++;
    });
    cartItems.innerHTML = completeProductPage;*/
    const cart = document.getElementById('shoppingCartContainer');
    let completeShoppingCart = "";

 //   let index = 0;

    let item;
    for (let i = 0; i < 6; i++) {
        if (sessionStorage.getItem('product'+i) !== null) { // niet leeg
            //console.log(JSON.parse(sessionStorage.getItem('product'+i)));
            item = JSON.parse(sessionStorage.getItem('product'+i));

            let cartItems =
                '<div class="col-md-4">' +
                '<div class="product-module">' +
                '<img onclick="productDetails(' + i + ')" class="card-img-top " src="../images/products/' + item.image + '.jpg"/>' + // image
                '<div class="product-name">' +
                '<h5><a href="#">' + item.productname + '</a> </h5>' + // naam
                '</div>' +
                '<div class="price"><h5>€ ' + item.price + '</h5></div>' +   // prijs
                '<div class="card-body">' +
                /*'<button onclick="addToBasket(' + index +')" class="btn btn-secondary">Add to Basket</button>' +*/
                '<button onclick="removeShoppingCartItem(' + i + ')" class="btn btn-outline-danger btn-xs">\n' +
                '<i class="fa fa-trash" aria-hidden="true"></i>\n</button>' +
                '</div>' +
                '</div>' +
                '</div>';
            completeShoppingCart += cartItems;

        }
    }
/*
    allProducts.forEach(product => {
        let productPage =
            '<div class="col-md-4">' +
            '<div class="product-module">' +
            '<img onclick="productDetails(' + index + ')" class="card-img-top " src="../images/products/' + product.image + '.jpg"/>' + // image
            '<div class="product-name">' +
            '<h5><a href="#">' + product.productname + '</a> </h5>' + // naam
            '</div>' +
            '<div class="price"><h5>€ ' + product.price + '</h5></div>' +   // prijs
            '<div class="card-body">' +
            '<button onclick="addToBasket(' + index +')" class="btn btn-secondary">Add to Basket</button>' +
            '</div>' +
            '</div>' +
            '</div>';
        completeProductPage += productPage;
        index++;
    });*/
    cart.innerHTML = completeShoppingCart;

}

function removeShoppingCartItem(index) {
    sessionStorage.removeItem('product' + index);
    location.reload();
}