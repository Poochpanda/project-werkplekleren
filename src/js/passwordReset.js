// deze file doet een post request naar de api met de waarden van de html onderdelen en voegt ze toe aan de database
// als email niet bestaat in de database, stuurt de backend een bevestiging (json)
// terug die confirmed of email bestaat of niet
window.onload = function () {
    const submitBtn = document.getElementById('submit');
    submitBtn.addEventListener('click', function (event) {
        event.preventDefault();
    });
    submitBtn.addEventListener('click', ()=> {
        putRequest();
    })

}

function putRequest() {
    if (sessionStorage.getItem('id') === null) {
        alert('Je moet ingelogd zijn om deze optie te gebruiken');
    } else {
        const newPassword = document.getElementById('newPassword').value;
        const confirmPassword = document.getElementById('confirmPassword').value;
        const data = {
            userId: sessionStorage.getItem('id'),
            newPassword: newPassword,
            confirmPassword: confirmPassword
        };

      //  fetch('https://localhost:44337/api/User?json=' + data, {
        fetch('https://localhost:44337/api/User',{
            method: 'put',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), // javascript to json
        })
            .then(validateResponse)
            .then(logResult)
            .then(logData)
            .catch(logError);
    }
}

function logResult(result) {
    return result.json();
}
function logData(result) {
  //  console.log(result.Data);
    alert(result.Data);
    document.location.href = '../html/homeBootstrap.html';
}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}