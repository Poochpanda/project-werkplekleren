window.onload = function () {
    const submitBtn = document.getElementById('submit');
    submitBtn.addEventListener('click', function (event) {
        event.preventDefault();
    })
    submitBtn.addEventListener('click', ()=> {
        login();
    })
}

function login() {
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    const data = {
        email: email,
        password: password
    };

    fetch('https://localhost:44337/api/User', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data), // javascript to json
    })
        .then(validateResponse)
        .then(logResult)
        .then(logData)
        .catch(logError);
}
function logResult(result) {
    return result.json();
}
function logData(result) { // uitloggen = sessionStorage leegmaken
    if (result.Data === 'Email of wachtwoord zijn niet juist ingevuld') {
        alert(result.Data);
    } else {                // login
            sessionStorage.setItem('id', result.Data);
            alert('Gebruiker met UserId: ' + sessionStorage.getItem('id') + ' ingelogd');
            document.location.href = '../html/homeBootstrap.html';
    }
}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

function mouseoverPass() {
    let obj = document.getElementById('password');
    obj.type = "text";
}
function mouseoutPass() {
    let obj = document.getElementById('password');
    obj.type = "password";
}
