// deze file doet een post request naar de api met de waarden van de html onderdelen en voegt ze toe aan de database
// als email niet bestaat in de database, stuurt de backend een bevestiging (json)
// terug die confirmed of email bestaat of niet
window.onload = function () {
    const submitBtn = document.getElementById('submit');
 //   submitBtn.preventDefault();
    submitBtn.addEventListener('click', ()=> {
        postRequest();
    })


}

function postRequest() {
    const password = document.getElementById("password").value;
    const newPassword = document.getElementById("confirmPassword").value;
    if (password !== newPassword) {
        alert("Je wachtwoord bevestiging komt niet overeen met je wachtwoord");
    } else {
        const email = document.getElementById("email").value;

        const firstname = document.getElementById("firstName").value;
        const lastname = document.getElementById("lastName").value;
        const birthdate = document.getElementById("birthDate").value;
        const data = {
            firstName: firstname,
            lastName: lastname,
            birthdate: birthdate,
            email: email,
            password: password,
        };

        fetch('https://localhost:44337/api/Account/', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data), // javascript to json
        })
            .then(validateResponse)
            .then(logResult)
            .then(logData)
            .catch(logError);
    }
}

function logResult(result) {
    return result.json();
}

function logData(result) {
    // email check, ipv window.alert moeten die waarden in een venster
    // komen te staan (zoals een div) zoals er gebeurt bij wachtwoord fout als je probeert in te loggen
    if (result.Data === "Email bestaat al") {
        window.alert("Er bestaat al een account met dit e-mailadres");
    } else if (result.Data === "Email aanvaard") {
        window.alert("Account aangemaakt!");
        document.location.href = '../html/login.html';
    }
}

function logError(error) {
    console.log('Looks like there was a problem:', error);
}

function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}