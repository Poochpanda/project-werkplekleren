// deze file doet een get request naar de database en krijgt alle persons terug
window.onload = function () {
    let submitButton = document.getElementById('submit');
    submitButton.addEventListener('click', () => {
        getUsers();
    })
};
function logResult(result) {
    console.log(result.Data); // hier staat de retourneerde data in atm

   // console.log(JSON.parse(result.data))
}
function logError(error) {
    console.log('Looks like there was a problem:', error);
}
function validateResponse(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
function readResponseAsJSON(response) {
    return response.json();
}
function getUsers() {
    // https://localhost:44397/api/Account/
    fetch('https://localhost:44337/api/Account/')
        .then(validateResponse)
        .then(readResponseAsJSON)
        .then(logResult)
        .catch(logError)
}